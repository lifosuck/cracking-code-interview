# Design a Juke Box using OOP

# assume the juke box takes a coin to play
# assume the juke box play a music selected by the user
# assume the juke box has a few states. 1. no coin, 2. coin inserted, but no music selected. 3. music selected but not playing.


class JukeboxState:
    Default = 0
    CoinInserted = 1
    SongSelected = 2
    Playing = 3


class Jukebox:
    def __init__(self, numb_of_songs):
        self.state = JukeboxState.Default
        self.num_of_songs = numb_of_songs
        self.song = None

    def return_coin(self, coin):
        return coin

    def insert_coin(self, coin):
        if self.state is not JukeboxState.Default:
            return self.return_coin(coin)
        if coin is not None:
            self.state = JukeboxState.CoinInserted

    def select_music(self, selection):

        if self.state is not JukeboxState.CoinInserted:
            raise Exception("insert coin or wait for the song playinig to be finished")

        if selection in range(self.num_of_songs):
            self.song = selection
            self.state = JukeboxState.SongSelected

    def play_song(self):
        print("playing song#", self.song)
        print("played song")
        self.song = None
        self.state = JukeboxState.Default

    def play_music(self):
        if self.state is JukeboxState.SongSelected:
            self.play_song()
