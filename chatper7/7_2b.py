# Design a Call Center

from enum import Enum
from random import randint

import asyncio


class EmployeeType(Enum):
    Respondent = 1
    Manager = 2
    Director = 3


class CallType(Enum):
    Unknown = 0
    Simple = 1
    Moderate = 2
    Difficult = 3


class Call:
    def __init__(self, caller):
        self.caller = caller,
        self.call_type = CallType.Unknown

    def get_type(self):
        return self.call_type

    def set_type(self, call_type):
        self.call_type = call_type


class CallQueue:
    def __init__(self):
        self.queue = []

    def add(self, call):
        self.queue.append(call)

    def pop(self):
        if self.queue:
            return self.queue.pop()

    def is_empty(self):
        if not self.queue:
            return True

    def pop_type(self, call_type):
        for index, call in enumerate(self.queue):
            if call.call_type is call_type:
                return self.queue.pop(index)

    def __iter__(self):
        return iter(self.queue)


class CallRerouter:
    def __init__(self, incoming_stream, unknown_call_stream, reroute_stream, short_cut_stream):
        self.incoming_stream = incoming_stream
        self.unknown_stream = unknown_call_stream
        self.reroute_stream = reroute_stream
        self.short_cut = short_cut_stream
        print(self.incoming_stream.queue)

    async def run(self):
        while True:
            call = self.incoming_stream.pop()
            if call:
                if call.get_type() is CallType.Unknown:
                    self.unknown_stream.add(call)
                else:
                    self.reroute_stream.add(call)
                continue
            if not self.short_cut.is_empty():
                for item in self.short_cut:
                    self.incoming_stream.queue.insert(0, item)
                    print("inserted back", item.call_type)
                self.short_cut = CallQueue()
            await asyncio.sleep(1)


class Employee:
    def __init__(self, employee_id, employee_type, incoming_call_stream, redirect_stream):
        self.id = employee_id
        self.employee_type = employee_type
        self.incoming_call_stream = incoming_call_stream
        self.redirect_stream = redirect_stream
        self.max_handled_type = CallType(employee_type.value)

    async def wait(self):
        await asyncio.sleep(randint(1, 10) / 1000)

    async def work(self):
        while True:
            if self.incoming_call_stream.is_empty():
                await self.wait()
                continue
            call = self.incoming_call_stream.pop_type(self.max_handled_type)
            if call is None:
                await self.wait()
                continue
            await self.handle_call(call)

    async def handle_call(self, call):
        print(self.id, " handled ", call, call.call_type, call.caller)
        await self.wait()


# respondent focus on unknown call types
class Respondent(Employee):
    def __init__(self, employee_id, incoming_call_stream, redirect):
        super().__init__(employee_id, EmployeeType.Respondent, incoming_call_stream, redirect)

    async def work(self):
        while True:
            if self.incoming_call_stream.is_empty():
                await self.wait()
                continue
            call = self.incoming_call_stream.pop()
            call = self.answer_call(call)
            if self.max_handled_type == call.get_type():
                await self.handle_call(call)
                continue
            self.redirect_stream.add(call)

    def answer_call(self, call):
        if call.get_type() is CallType.Unknown:
            call_type = CallType(randint(CallType.Simple.value, CallType.Difficult.value))
            call.set_type(call_type)
            return call
        return call


# manager and director focus on known call types

class Manager(Employee):
    def __init__(self, employee_id, incoming_call_stream, redirect):
        super().__init__(employee_id, EmployeeType.Manager, incoming_call_stream, redirect)


class Director(Employee):
    def __init__(self, employee_id, incoming_call_stream, redirect):
        super().__init__(employee_id, EmployeeType.Director, incoming_call_stream, redirect)


class EmployeesController:
    def __init__(self):
        self.employees = {}

    def add_employee(self, employee):
        self.employees[employee.id] = employee

    def get_employees(self):
        return [employee for _, employee in self.employees.items()]


def main():
    incoming_call_queue = CallQueue()
    for i in range(1000):
        incoming_call_queue.add(Call(caller="hello this is msg{0}".format(i)))
    unknown_queue = CallQueue()
    reroute_queue = CallQueue()
    short_cut_queue = CallQueue()
    employees = EmployeesController()

    employees.add_employee(Respondent(2, unknown_queue, short_cut_queue))
    employees.add_employee(Respondent(1, unknown_queue, short_cut_queue))
    employees.add_employee(Respondent(3, unknown_queue, short_cut_queue))
    employees.add_employee(Manager(4, reroute_queue, short_cut_queue))
    employees.add_employee(Manager(5, reroute_queue, short_cut_queue))
    employees.add_employee(Director(6, reroute_queue, short_cut_queue))
    loop = asyncio.get_event_loop()
    for emp in employees.get_employees():
        asyncio.ensure_future(emp.work())

    call_center = CallRerouter(incoming_call_queue, unknown_queue, reroute_queue, short_cut_queue)

    asyncio.ensure_future(call_center.run())
    loop.run_forever()


if __name__ == "__main__":
    main()
