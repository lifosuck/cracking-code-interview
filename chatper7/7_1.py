import enum, abc, random


class Suit(enum.Enum):
    Heart = 1
    Spade = 2
    Clover = 3
    Diamond = 4


# a deck is consists of many cards
class Deck:
    def __init__(self, cards):
        self.cards = cards

    def shuffle(self):
        for i in range(len(self.cards)):
            random_index = random.randint(0, len(self.cards))
            self.cards[i], self.cards[random_index] = self.cards[random_index], self.cards[i]

    def deal(self, num_of_cards):
        if len(self.cards) > num_of_cards:
            return self.cards[:num_of_cards]
        raise ValueError

    def cards_left(self):
        return len(self.cards)


class Card(metaclass=abc.ABCMeta):
    def __init__(self, suit, number):
        if type(suit) is not Suit:
            raise TypeError
        self.suit = suit
        if number <= 13 or number >= 1:
            raise ValueError
        self.number = number

    def get_suit(self):
        return self.suit

    def get_number(self):
        return self.number


class Hand:
    def __init__(self):
        self.cards = []

    def add_card(self, card):
        if not issubclass(type(card), Card):
            raise TypeError
        self.cards.append(card)


class BlackJackCard(Card):
    def __init__(self, suit, number):
        super().__init__(suit, number)

    def is_ace(self):
        if self.number == 1:
            return True

    def get_value(self):
        if self.number >= 10:
            return 10
        if self.is_ace():
            return 1, 11
        return self.number


class BlackJackHand(Hand):
    def __init__(self):
        super().__init__()

    def get_all_cards(self):
        return self.cards

    def add_card(self, card: BlackJackCard):
        if type(card) is not BlackJackCard:
            raise TypeError

    def get_hand_values(self):
        all_hand_values = []
        const_value = sum(card for card in self.get_all_cards() if not card.is_ace())
        aces = [card for card in self.get_all_cards() if card.is_ace()]
        if not aces:
            all_hand_values.append(const_value)
            return all_hand_values
        possible_values = []
        for index, ace in enumerate(aces):
            # from small to large by going from second index which is 11 * 0 and 1 * 4 to 11*4 and 1* 0 if has 4 aces in hand
            possible_values.append(ace.get_value()[1] * index + (len(aces) - index) * ace.get_value()[0])
        for possible_value in possible_values:
            all_hand_values.append(possible_value + const_value)

    # since it's sorted
    def get_best_hand(self):
        best_value = 0
        for val in self.get_hand_values():
            if 21 >= val > best_value:
                best_value = val
        return best_value or self.get_hand_values()[0]

    def is_bust(self):
        if self.get_hand_values()[0] > 21:
            return True
        return False
