from abc import ABCMeta, abstractmethod
from enum import Enum
from random import randint

# i don't like the design from the book since their call handler references, and keep tracks of employees, and their employees need reference to the actual call center.


class CallType(Enum):
    Simple = 1
    Moderate = 2
    Difficult = 3


class EmployeeType:
    Respondent = 1
    Manager = 2
    Director = 3


class CallCenter:
    def __init__(self, employees):
        self.employees = employees
        self.employees_directory = {}

    def initialize_directory(self):
        for employee in self.employees:
            if employee.type in self.employees_directory:
                self.employees_directory[employee.type].append(employee)
            else:
                self.employees_directory[employee.type] = [employee]

    # 1. must be handled by responders, then handle by manager, then handle by director
    def dispatch_call(self, call):


        pass


class Call:
    def __init__(self, call_type):
        if not isinstance(call_type, CallType):
            raise TypeError
        self.call_type = call_type
        self.duration = randint(0, 10)
        self.is_handled = False

    def get_type(self):
        return self.call_type

    def is_handled(self):
        return self.is_handled

    def handle(self):
        self.is_handled = True


class CallCenterEmployees(metaclass=ABCMeta):
    def __init__(self, title):
        self.title = title
        self.busy = False

    @abstractmethod
    def handle_call(self, call):
        pass

    def is_busy(self):
        return self.busy


class Respondent(CallCenterEmployees):
    def __init__(self):
        super().__init__(EmployeeType.Respondent)

    def handle_call(self, call):
        if self.is_busy():
            return
        if call.get_type() is CallType.Simple:
            self.busy = True
            # to simulate, need async , need to pass in the operator as callback
            call.handle()
            self.busy = False
            return
