# simple implementation of a linked list and a test to use the linked list

class LinkedList():
    
    def __init__(self):
        self.root = None
        self.head = None
    
    def __str__(self):
        return f"{self.__repr__()}  \n{str([n for n in self])}"

    def __iter__(self):
        # return self
        return self.gen()

    def gen(self):

        current = self.root
        while(current):
            yield current.value
            current = current.next

    # def __next__(self):
        # if (self.root.next):
            # return self.root.next
        # raise StopIteration() 

    def insert(self,index,item):
        pass
    
    def add(self,item):
        
        node = Node(item)
        if(self.root):
            self.head.point(node)
            self.head = node
        else:
            self.root = node     
            self.head = node

    
    def pop(self):
        start = self.root
        prev = None
        if(start == None):
            return

        elif(not start.next):
            # pop the only item
            self.root = None
            self.head = None
            return

        while (True):
            prev = start #book keep the prev item
            start = start.next # current item
            if(not start.next): #if current item doesn't point to anything
                prev.point(None) #then current item is last node, then remove prev item
                self.head = prev
                break;

    def get(self,index):
        # if index exceeds limit it will throw error
        startingNode = self.root
        for i in range(index):
            startingNode = (startingNode.next)
        return startingNode.value 

    def remove(self,index):
        pass

    def copy(self):
        pass

class Node():   

    def __init__(self,value):
        self.next = None
        self.value = value

    def point(self,ptr):
        self.next = ptr
    
    def set(self,value):
        self.value = value
        
    def reset(self):
        self.next = None
        self.value = None

def main():
    print("running program")
    a = LinkedList()
    a.add(5)
    a.add(4)
    a.add(3)
    a.add(2)
    
    print(a)
    print(a.get(1))
    a.pop()
    a.pop()
    a.pop()
    a.pop()
    print(a)
    pass




if __name__ == "__main__":
    main()
