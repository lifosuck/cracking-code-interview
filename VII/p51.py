"""
the purpose of this program is the write a better algorithm to display or count the number of possible permutations than the crazy implementation by the book
"""
ITEMS = [1,2,3,4,5,6]


def find_perm(items):
    if len(items) == 1:
        return [items]
    elif len(items) == 0:
        return []
    else:
        final_list = []
        # origitems = cloneList(items)

        for i, item in enumerate(items):
            newlist = cloneList(items)
            newlist.pop(i)
            for perm in find_perm(newlist):
                itemlist = [item, *(n for n in perm)]
                # itemlist.extend(perm)
                final_list.append(itemlist)

        return final_list


def cloneList(item):
    return item[:]



def main():
    testList = find_perm(ITEMS)
    print(testList)
    print(len(testList))



    pass

if __name__ == "__main__":
    main()
