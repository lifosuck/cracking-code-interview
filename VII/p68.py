"""
the purpose of the program is to calculate all values of a^3+b^3 =c^3 + d^3 
where a,b,c,d are within 1000
"""
N = 1000
import datetime


def main():
    # for each a and b , they are a set of values, calculate their possible values
    print("running main")
    results = {}
    first_start = datetime.datetime.now()
    for n in range(N):
        for i in range(N):
            comb = (i,n)
            calc_val = i**3 + n**3
            if calc_val in results:
                results[calc_val].append(comb)
            else:
                results[calc_val] = [comb]
    
    for k,v in results.items():
        if len(v) > 1:
            pass
            # print(f"{k} is the sum between the set of {v}")

    # this finds all solutions including trival solutions
    first_end = datetime.datetime.now()

    second_start = datetime.datetime.now() 
    # this solution finds the non trival solution where a^3 + b^3 = (c)=b^3 + (d)=a^3 and  (c)=a^3 + (d)=b^3 
    for n in range(N):
        for i in range(n,N):
            comb = (i,n)
            calc_val = i**3 + n**3
            if calc_val in results:
                results[calc_val].append(comb)
            else:
                results[calc_val] = [comb]

    for k,v in results.items():
        if len(v) > 1:
            pass
   #         print(f"{k is the sum between sets of {v}")


    second_end = datetime.datetime.now() 
    
    print(f"first run time = {first_end-first_start} second runtime = {second_end - second_start}") 

if __name__ == "__main__": 
    main()

