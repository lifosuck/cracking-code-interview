# single linked list

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class SingleLinkedList:
    def __init__(self):
        self.root = None
        self.ptr = None

    def add(self, data):
        head = self.find_head()
        node = Node(data)
        if head is None:
            self.root = node
        else:
            head.next = node
        return node

    def len(self):
        if not self.root:
            return 0
        current_node = self.root
        counter = 1
        while True:
            if current_node.next is None:
                return counter
            else:
                current_node = current_node.next
                counter += 1

    def delete(self, index):
        if index == 0:
            self.root = self.root.next
        prev_node = self.get(index - 1)
        next_node = prev_node.next.next
        if next_node is None:
            prev_node.next = None
        else:
            prev_node.next = next_node

    # get by index, where 0 is the root
    def get(self, index):
        if self.root is None and index == 0:
            return None
        node = self.root
        for _ in range(index):
            node = node.next
        return node

    # find head returns the head node, if empty, returns back None
    def find_head(self):
        if not self.root:
            return None
        current_node = self.root
        while True:
            if current_node.next is None:
                return current_node
            current_node = current_node.next
