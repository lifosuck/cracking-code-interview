# Remove Duplicates in an unsorted linked list
import unittest
from chapter2.single_linked_list import SingleLinkedList


# Algorithm complexity O(N) since it has to go through each item once , removal takes O(1) speed, appending to hash takes 1step
class MyTest(unittest.TestCase):
    def test(self):
        linked_list_example = SingleLinkedList()
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(2)
        delete_duplciates(linked_list_example)
        self.assertEqual(linked_list_example.len(), 2)
        self.assertEqual(linked_list_example.get(0).data, 1)
        self.assertEqual(linked_list_example.get(1).data, 2)

        linked_list_example = SingleLinkedList()
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(1)
        linked_list_example.add(2)
        delete_duplicates_no_hash(linked_list_example)
        self.assertEqual(linked_list_example.len(), 2)
        self.assertEqual(linked_list_example.get(0).data, 1)
        self.assertEqual(linked_list_example.get(1).data, 2)


def delete_duplciates(linked_list):
    temp_hash = {}
    root_node = linked_list.root
    current_node = root_node
    prev_node = None
    while True:
        if current_node is None:
            # done, reached the end
            break
        if current_node.data in temp_hash:
            # remove current node
            remove_current_node(prev_node, current_node)
        else:
            temp_hash[current_node.data] = True
            prev_node = current_node
        current_node = current_node.next


def delete_duplicates_no_hash(linked_list):
    root_node = linked_list.root
    current_node_to_be_checked = root_node
    current_node = root_node.next
    prev_node = root_node
    while True:
        if current_node_to_be_checked is None or current_node is None:
            # done, reached the end
            break
        # check the current node is duplicate by going through current node to the last
        while True:
            if current_node.next is None:
                break
            if current_node.data == current_node_to_be_checked.data:
                remove_current_node(prev_node, current_node)
            else:
                prev_node = current_node
            current_node = current_node.next

        current_node_to_be_checked = current_node_to_be_checked.next
        current_node = current_node_to_be_checked.next
        prev_node = current_node_to_be_checked



def is_duplicate(node):
    next = node.next
    while next is not None:
        if next.data == node.data:
            return True
        next = next.next
    return False


def remove_current_node(prev, current):
    next_node = current.next
    if next_node is not None:
        prev.next = next_node
    else:
        prev.next = None
