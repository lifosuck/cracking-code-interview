from chapter2.single_linked_list import SingleLinkedList


def main():
    single_linked_list = SingleLinkedList()
    single_linked_list.add(1)
    single_linked_list.add(2)
    single_linked_list.add(3)
    print(single_linked_list.len())
    single_linked_list.delete(0)
    print(single_linked_list.len())
    print(single_linked_list.get(0).data)


if __name__ == "__main__":
    main()
