# doubly linked list

class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None


class DoubleLinkedList:
    def __init__(self):
        self.root = None

    def find_head(self):
        if not self.root:
            return None
        head = self.root
        while True:
            if head.next is None:
                return head
            head = head.next

    def add(self, value):
        head = self.find_head()
        node = Node(value)
        if not head:
            self.root = node
        else:
            head.next = node
            node.prev = head

    def get(self, index):
        if index == 0:
            return self.root
        count = index
        current_node = self.root
        for _ in range(count):
            current_node = current_node.next
        return current_node

    def delete(self, index):
        node = self.get(index)
        if node is None:
            return
        if node.next is None:
            node.prev.next = None
        else:
            node.prev = node.next
