from pprint import pprint
from flask import jsonify
from flask import Flask, session, request
from flask_session import Session
from chapter7.minesweeper import Game
import redis
import uuid

SESSION_TYPE = 'redis'
r = redis.StrictRedis(host='localhost', port=6379, db=0)
SESSION_REDIS = r
app = Flask(__name__)
game = {}
app.config.from_object(__name__)

app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

Session(app)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', 'http://localhost:8080')
    # response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    return response


@app.route('/restart', methods=['GET', 'POST', 'OPTIONS'])
def restart():
    if request.method == 'OPTIONS':
        return 'ok'
    req = request.get_json()
    col = req['col']
    row = req['row']
    mines = req['mines']

    if 'us' in session:
        if session['us'] not in game:
            game[session['us']] = Game(row, col, mines)
        game[session['us']].height = row
        game[session['us']].width = col
        game[session['us']].mines = mines
        game[session['us']].restart()
    else:
        session['us'] = str(uuid.uuid4())
        game[session['us']] = Game(row, col, mines)
    game_obj = game[session['us']]
    if 'game' not in session:
        session['game'] = game_obj
    print(session['game'])
    response = jsonify(
        {"result": game_obj.print_board(False), "game_state": game_obj.state.name, "mines": game_obj.mines,
         "pieces_revealed": game_obj.pieces_revealed}, )

    return response


@app.route('/step_on', methods=['GET', 'POST', 'OPTIONS'])
def step_on():
    if request.method == 'OPTIONS':
        return 'ok'
    print(session)
    req = request.get_json()
    col = req['col']
    row = req['row']
    if 'us' in session:
        print(session['us'])
        if session['us'] not in game:
            game[session['us']] = Game(10, 10, 10)
        game[session['us']].step_on(row, col)
    else:
        session['us'] = str(uuid.uuid4())
        game[session['us']].step_on(row, col)
    game_obj = game[session['us']]
    response = jsonify(
        {"result": game_obj.print_board(False), "game_state": game_obj.state.name, "mines": game_obj.mines,
         "pieces_revealed": game_obj.pieces_revealed}, )

    return response


@app.route('/toggle_flag', methods=['GET', 'POST', 'OPTIONS'])
def mark():
    if request.method == 'OPTIONS':
        return 'ok'
    req = request.get_json()
    col = req['col']
    row = req['row']
    if 'us' in session:
        print(session['us'])
        if session['us'] not in game:
            game[session['us']] = Game(10, 10, 10)
        game[session['us']].mark(row, col)
    else:
        session['us'] = str(uuid.uuid4())
        game[session['us']].mark(row, col)
    pprint(game[session['us']].print_board(False))
    game_obj = game[session['us']]
    response = jsonify(
        {"result": game_obj.print_board(False), "game_state": game_obj.state.name, "mines": game_obj.mines,
         "pieces_revealed": game_obj.pieces_revealed}, )

    return response


if __name__ == "__main__":
    app.run()
