def main():
    # print(generate_parens(0))
    # print(generate_parens(1))
    # print(generate_parens(2))
    # print(generate_parens(3))
    # print(generate_parens(4))
    #
    # print(len(generate_parens(4)))
    list1 = generate_parens(5)
    # print(len(generate_parens(6)))
    # print(len(generate_parens(7)))
    list2 = generate_parens2(5)
    map1 = {}
    for item in list1:
        map1[item] = True
    print(len(list1))
    print(len(map1))
    map2 = {}
    for item in list2:
        map2[item] = True
        if item not in list1:
            print("item" + item + " is not in prev list")

    print(len(list2))
    print(len(map2))


def append_paren(overall_list, n_size, prev, left, right, index):
    if index == n_size * 2:
        return overall_list.append(prev)
    if right > left:
        return None

    if left < n_size:
        append_paren(overall_list, n_size, prev + "(", left + 1, right, index + 1)
    if right < left and right < n_size:
        append_paren(overall_list, n_size, prev + ")", left, right + 1, index + 1)


def generate_parens2(num_of_pairs):
    my_list = []
    append_paren(my_list, num_of_pairs, "", 0, 0, 0)
    print(my_list)
    return my_list


def generate_parens(num_of_pairs):
    if num_of_pairs == 0:
        return ""
    if num_of_pairs == 1:
        return "()"
    if num_of_pairs == 2:
        return "()()", "(())"
    if num_of_pairs > 2:
        overall_solution = []
        for sol in generate_parens(num_of_pairs - 1):
            temp_left = "()" + sol
            temp_right = sol + "()"
            temp_center = f"({sol})"
            if temp_left != temp_right:
                overall_solution.append(temp_left)
            overall_solution.append(temp_right)
            overall_solution.append(temp_center)
        return overall_solution


if __name__ == "__main__":
    main()
