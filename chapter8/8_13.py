# stack of boxes
class Box:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.unique = (x, y, z)

    def __repr__(self):
        return f'{self.x},{self.y},{self.z}'


def main():
    box1 = Box(10, 10, 10)
    box2 = Box(9, 11, 10)
    box3 = Box(6, 6, 6)
    box4 = Box(5, 3, 5)
    boxes = [box1, box2, box3, box4]
    sorted_boxes = sorted(boxes, key=lambda box: box.x, reverse=True)
    result = find_tallest_combination_of_boxes(boxes)
    print(result)


pass


def find_tallest_combination_of_boxes(boxes):
    # take the first few with the same edge length
    if not boxes:
        return
    if len(boxes) == 1:
        return boxes
    largest = boxes[0]
    smaller_boxes = []
    large_box = []
    for index,box in enumerate(boxes):
        if box.x < largest.x:
            smaller_boxes.append(box)
        else:
            large_box.append(box)
    for box in large_box:
        



if __name__ == "__main__":
    main()
