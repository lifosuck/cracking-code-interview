# robot in a grid
from pprint import pprint
from random import randint

r = 10
c = 10

r_row = []
for i in range(r):
    col = []
    for j in range(c):
        col.append(0)

    col[randint(0, c - 1)] = randint(0, 1)
    col[randint(0, c - 1)] = randint(0, 1)
    col[randint(0, c - 1)] = randint(0, 1)
    col[randint(0, c - 1)] = randint(0, 1)
    col[randint(0, c - 1)] = randint(0, 1)
    col[randint(0, c - 1)] = randint(0, 1)
    col[randint(0, c - 1)] = randint(0, 1)
    col[randint(0, c - 1)] = randint(0, 1)
    col[randint(0, c - 1)] = randint(0, 1)
    col[randint(0, c - 1)] = randint(0, 1)
    r_row.append(col)


class Position:
    def __init__(self, r, c):
        self.location = (r, c)
        self.path = []

    def set_path(self, path):
        self.path = path


def is_valid(row, col):
    if 0 <= row < r and 0 <= col < c and r_row[row][col] == 0:
        return True
    return False


# given a coordinate, create a current position if not already in map, and continue traverse
def explore(current, parent, end, stored):
    if current in stored:
        return
    current_position = Position(current[0], current[1])
    parent_path = parent.path[:]
    parent_path.append(current_position.location)
    current_position.set_path(parent_path)
    stored[current_position.location] = current_position
    if current_position.location == end.location:
        return current_position.path

    down = (current[0] + 1, current[1])
    right = (current[0], current[1] + 1)
    if is_valid(down[0], down[1]):
        result = explore(down, current_position, end, stored)
        if result:
            return result
    if is_valid(right[0], right[1]):
        result = explore(right, current_position, end, stored)
        if result:
            return result


def main():
    fake_parent = Position(0, 0)
    end = Position(r - 1, c - 1)
    stored = {}
    result = explore((0, 0), fake_parent, end, stored)
    pprint(r_row)
    print(result)


if __name__ == "__main__":
    main()
