# Triple step

num_of_steps_look_up = {
    0: 0,
    1: 1,
    2: 2,
    3: 4,
}


def number_of_steps(steps):
    if steps == 0:
        return 0
    if steps == 1:
        return 1
    if steps == 2:
        return 2
    if steps == 3:
        return 4
    if steps > 3:
        return number_of_steps(steps - 1) + number_of_steps(steps - 2) + number_of_steps(steps - 3)


def number_of_steps2(steps):
    if steps not in num_of_steps_look_up:
        num_of_steps_look_up[steps] = number_of_steps2(steps - 1) + number_of_steps2(steps - 2) + number_of_steps2(
            steps - 3)
    return num_of_steps_look_up[steps]


def main():
    # print(number_of_steps(20))
    print(len(str(number_of_steps2(100))))


if __name__ == "__main__":
    main()
