# tower of hanoi

# three towers. tower1, tower2 tower3, starting from tower1, move N discs to tower 3
count = 0


class Tower:
    def __init__(self, size, tower_name):
        self.queue = []
        self.name = tower_name
        for n in range(size):
            self.queue.append(size - n)

    def add(self, val):
        self.queue.append(val)

    def remove(self):
        return self.queue.pop()


def move_single_disc(start, end):
    global count
    count += 1
    val = start.remove()
    end.add(val)
    # print(f"moving disc from {start.name} to {end.name} ")


def move_discs_from_towers(size, start, buffer, end):
    if size <= 0:
        return
    move_discs_from_towers(size - 1, start, end, buffer)
    move_single_disc(start, end)
    move_discs_from_towers(size - 1, buffer, start, end)


def main():
    print(count)
    tower1 = Tower(7, "tower Start")
    tower2 = Tower(0, "tower buffer")
    tower3 = Tower(0, "tower end")
    move_discs_from_towers(7, tower1, tower2, tower3)
    print(count)


if __name__ == "__main__":
    main()
