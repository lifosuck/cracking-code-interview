# coins
all_possible_coins = [1, 5, 10, 25]

#todo should redo, using memoization, also going from most denomination to lower denomination

def main():
    print(find_ways_to_represent(1))
    print(find_ways_to_represent(3))
    print(find_ways_to_represent(5))
    print(find_ways_to_represent(6))
    print(find_ways_to_represent(25))
    pass


def find_ways_to_represent(value):
    if value == 0:
        return [[]]
    if value == 1:
        return [[1]]
    ways = []
    for coin in all_possible_coins:
        if value // coin >= 1:
            for way in find_ways_to_represent(value - coin):
                way.append(coin)
                ways.append(way)

    return ways


if __name__ == "__main__":
    main()
