# permutation with duplication


# eg A A B

def main():
    test_string = "AABBB"
    result = find_permutation(test_string)
    print(result)
    pass


def find_permutation(some_string):
    if len(some_string) == 0:
        return
    if some_string[0] * len(some_string) == some_string:
        return [some_string]
    if len(some_string) == 2:
        return some_string[0] + some_string[1], some_string[1] + some_string[0]
    result = []
    hash = {}
    for i in range(len(some_string)):
        if some_string[i] in hash:
            continue
        for string in find_permutation(some_string[:i] + some_string[i + 1:]):
            result.append(some_string[i] + string)
        hash[some_string[i]] = True
    return result


if __name__ == "__main__":
    main()
