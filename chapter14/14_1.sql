select t.tenant_name, t.tenant_id, count(ta.apartment_id) as total_count
from tenants as t left join tenant_apartments ta on ta.tenant_id = t.tenant_id
group by t.tenant_id
having count(ta.apartment_id) > 1
order by total_count  desc 