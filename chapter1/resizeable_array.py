# make an resizable array that handles append/ pop in O(1) amortized time

class Null:
    pass


class ResizeableArray:
    def __init__(self):
        self.internal_array = []
        self.size = 0

    def __str__(self):
        return str(self.internal_array)

    def capacity(self):
        return len(self.internal_array)

    def append(self, obj):
        self.insert(self.size, obj)

    def __iter__(self):
        return iter(self.internal_array)

    def insert(self, index, obj):
        self.size += 1  # new size of the  array
        if self.size > self.capacity():
            self.resize(2 * (self.capacity() or 1))
        if index < self.size - 1:
            for i in reversed(range(index, self.size)):
                self.internal_array[i + 1] = self.internal_array[i]
        self.internal_array[index] = obj

    def reinitialize(self, size):
        return [Null() for _ in range(size)]

    def resize(self, size):
        newArray = self.reinitialize(size)
        for i in range(min(size, self.capacity())):
            newArray[i] = self.internal_array[i]
        self.internal_array = newArray

    def len(self):
        return self.size

    def pop(self):
        temp = self.internal_array[self.size - 1]
        self.delete(self.size - 1)
        return temp

    def delete(self, index):
        self.size -= 1
        if self.size < self.capacity() // 4:
            self.resize(self.capacity() // 2)
        for i in range(index, self.size):
            self.internal_array[i] = self.internal_array[i + 1]
        self.internal_array[self.size] = Null()



