# Check Permutation: given two strings determine if one is permutation of the other
import unittest
from chapter1.hashmap import TrivialHash

calls = 0


class MyTest(unittest.TestCase):
    def test(self):
        global calls
        self.assertTrue(brute_force("hiyabcdefg", "fhyaicdegb"))
        print(calls)
        calls = 0
        self.assertTrue(count_occurence("hiyabcdefg", "ihyabcdegf"))
        print(calls)
        calls = 0
        self.assertFalse(brute_force("hi", "oy"))
        print(calls)
        calls = 0
        self.assertFalse(count_occurence("hi", "oy"))
        print(calls)


# brute force
# this is O(n!) time complexity and O(n) space complexity
def get_combination(string):
    global calls
    calls += 1
    if len(string) <= 1:
        return string
    if len(string) == 2:
        return string[0] + string[1], string[1] + string[0]
    if len(string) > 2:
        iters = []
        for index, c in enumerate(string):
            toIterate = string[index]
            toFindCombination = string[:index] + string[index + 1:]
            for comb in get_combination(toFindCombination):
                iters.append(toIterate + comb)
        return iters


def brute_force(string1, string2):
    if len(string1) != len(string2):
        return False
    for combination in get_combination(string1):
        if combination == string2:
            return True
    return False


# counter the occurence
def count_occurence(string1, string2):
    global calls
    if len(string1) != len(string2):
        return False
    hash = TrivialHash()
    for c in string1:
        calls += 1
        if hash.get(c) is None:
            hash.append(c, 1)
        else:
            hash.append(c, hash.get(c) + 1)
    for char in string2:
        calls += 1
        if hash.get(char) is None:
            return False
        elif hash.get(char) == 1:
            hash.delete(char)
        elif hash.get(char) > 1:
            hash.append(char, hash.get(char) - 1)
    if hash.size == 0:
        return True
    return False
# text book idea : map char to number, and have an array, accessing array is O(1) thus the whole algorithm is O(n)
