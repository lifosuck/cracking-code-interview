# String Compression
import unittest


# this algorithm is O(N)
class MyTest(unittest.TestCase):
    def test(self):
        self.assertEqual(string_compress("aabcccccaaa"), "a2b1c5a3")
        self.assertEqual(string_compress("a"), "a")
        self.assertEqual(string_compress("aa"), "aa")
        self.assertEqual(string_compress("aaa"), "a3")


def string_compress(input):
    maxLength = len(input)
    compressedMsg = ""
    currChar = ""
    currCount = 0
    for index, c in enumerate(
            input + " "):  # add one end of line char so it knows to terminate and concat the last set of values into the compressed msg
        if c != currChar:
            if currChar != "":
                compressedMsg += f"{currChar}{currCount}"
            currChar = c
            currCount = 1
        else:
            currCount += 1
        if len(compressedMsg) >= maxLength:
            return input

    return compressedMsg
