# Zero Matrix, search through an MxN matrix and if an element is 0, the entire row and column are set ot 0
import unittest


# Algorithm complexity ie. this is O(M*N) time algorithm (because worst case, it needs to scan all and none has 0 values

class MyTest(unittest.TestCase):
    def test(self):
        test_matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 0]]
        expected_result = [[1, 2, 0], [4, 5, 0], [0, 0, 0]]
        self.assertEqual(zero_matrix(test_matrix), expected_result)


def zero_matrix(input):
    rows_to_be_zeroed = {}
    columns_to_be_zeroed = {}
    for row_index, row in enumerate(input):
        for col_index, val in enumerate(row):
            if val == 0:
                rows_to_be_zeroed[row_index] = True
                columns_to_be_zeroed[col_index] = True

    # find out all places that need to be zeroed
    for row_index, row in enumerate(input):
        for col_index, val in enumerate(row):
            if row_index in rows_to_be_zeroed or col_index in columns_to_be_zeroed:
                input[row_index][col_index] = 0
    return input

#todo i think the text book has incorrect solution, if they used the first row an column to keep track which needs to be zeroed
#then if they are not zero, the values need to recovered. there is no way for them to recover it unless they don't do it in place which is retarded, and how they set the rows and columns, most likely they'll double set for matrix that is all 0s


