# Palindrome Permutation
import unittest
from chapter1.hashmap import TrivialHash


# this is O(N) space/time algorithm
class MyTest(unittest.TestCase):
    def test(self):
        self.assertTrue(is_palindrome_permutation("taco cat"))


def is_palindrome_permutation(string):
    hashmap = TrivialHash()
    for c in string:
        if c == " ":
            continue
        if not hashmap.get(c):
            hashmap.append(c, 1)
        else:
            hashmap.append(c, hashmap.get(c) + 1)

    odd_occurrence = 0

    for k, v in hashmap.items():
        if v % 2 != 0:
            odd_occurrence += 1

    if odd_occurrence > 1:
        return False
    return True
