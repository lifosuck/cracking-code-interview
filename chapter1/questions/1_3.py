# URLify: replace strings in all spaces in string with "%20", given true length of the string and assume enough space in
# the array
import unittest
from chapter1.string_builder import StringBuilder

# this algorithm is O(N) speed, but space is a little big bigger if we do in place array manipulation where the time complexity is O(N^2)

class MyTest(unittest.TestCase):
    def test(self):
        self.assertEqual(URLify("Mr John Smith    ", 13), "Mr%20John%20Smith")


def URLify(string, true_length):
    string_builder = StringBuilder()

    for i in string:
        if true_length == 0:
            break
        true_length -= 1
        if i == " ":
            string_builder.concatenate("%20")
        else:
            string_builder.concatenate(i)
    return str(string_builder)
