# Name of the question, and short description
import unittest


# Algorithm complexity ie. this is O(N) space/time algorithm
class MyTest(unittest.TestCase):
    def test(self):
        test_matrix = [["11", "12", "13"], ["21", "22", "23"], ["31", "32", "33"]]
        # new_matrix = modify_matrix(test_matrix)
        # self.assertEqual(new_matrix[0][0], "13")
        # print( [n for n in generate_indexes_to_transform(5)])
        new_matrix = modify_matrix_in_place(test_matrix)
        print(new_matrix)


def modify_matrix(input):
    new_matrix = []
    n = len(input)
    for h, rows in enumerate(input):
        new_row = []
        for l, row in enumerate(rows):
            new_i, new_j = transform_access(h, l, n)
            new_row.append(input[new_i][new_j])
        new_matrix.append(new_row)
    return new_matrix


total_sides = 4


def modify_matrix_in_place(input):
    size = len(input)
    g = generate_indexes_to_transform(size)
    for i, j in g:
        rotate_item(input, i, j)
    return input


# get starting point,
# find the next point,
# take the value of the next point, save it in temp
# next point becomes the new point, find the next point,

# A B
#  D  C
#=>
# B C
# A D

# A , B , C ,D
# A , A , C, D (temp B)
# A , A , B, D (temp C)
# A,  A, B, C (temp D)
# D , A , B, C (temp A)
def rotate_item(input, i, j):
    for _ in range(total_sides - 1):
        new_i, new_j = transform_access(i, j, len(input))
        input[i][j] = input[new_i][new_j]
        i = new_i
        j = new_j


# given size n, generate all the index items that need to rotate/swap 4 times
def generate_indexes_to_transform(n):
    # for n = 3, 00, 01, 11
    # for n =4 , 00, 01, 02, 11
    depth = n // 2

    # xxxx
    # xxxx
    # xxxx

    # yyyx
    # xyxx
    # xxxx

    # yyyyx
    # xyyxx
    # xxxxx
    # xxxxx
    # xxxxx
    for i in range(depth):
        for j in range(i, n - 1 - i):
            yield i, j


def transform_access(i, j, n):
    # n is size of N x N array
    new_i = j
    new_j = n - i - 1
    print(new_i, new_j)
    return new_i, new_j
