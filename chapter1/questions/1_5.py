# One Way: check if edits can be performed on one string to be equal to the other string
import unittest


# Algorithm complexity ie. this is O(N) time algorithm
class MyTest(unittest.TestCase):
    def test(self):
        self.assertTrue(is_string_at_most_one_edit_away("pale", "ple"))
        self.assertTrue(is_string_at_most_one_edit_away("pales", "pale"))
        self.assertTrue(is_string_at_most_one_edit_away("pale", "bale"))
        self.assertFalse(is_string_at_most_one_edit_away("pale", "bake"))

        self.assertTrue(is_string_at_most_one_edit_away_iterative("pale", "ple"))
        self.assertTrue(is_string_at_most_one_edit_away_iterative("pales", "pale"))
        self.assertTrue(is_string_at_most_one_edit_away_iterative("pale", "bale"))
        self.assertFalse(is_string_at_most_one_edit_away_iterative("pale", "bake"))


def is_same(input1, input2):
    if len(input1) != len(input2):
        return False
    for index, _ in enumerate(input1):
        if input1[index] != input2[index]:
            return False
    return True


def is_string_at_most_one_edit_away(input1, input2):
    # if more than 1 char length, not the same
    if abs(len(input1) - len(input2)) > 1:
        return False

    if max(len(input1), len(input2)) == 1:
        return True
    # if 1 length difference
    if len(input1) != len(input2):

        less = input1
        more = input2
        if len(input1) > len(input2):
            more = input1
            less = input2

        if input1[0] == input2[0]:
            return is_string_at_most_one_edit_away(input1[1:], input2[1:])
        if input1[0] != input2[0]:
            return is_same(less, more[1:])

    # if 0 length difference
    if len(input1) == len(input2):
        if input1[0] == input2[0]:
            return is_string_at_most_one_edit_away(input1[1:], input2[1:])
        return is_same(input1[1:], input2[1:])


def is_string_at_most_one_edit_away_iterative(input1, input2):
    # if more than 1 char length, not the same
    if abs(len(input1) - len(input2)) > 1:
        return False

    if max(len(input1), len(input2)) == 1:
        return True

    if len(input1) != len(input2):
        less = input1
        more = input2
        if len(input1) > len(input2):
            more = input1
            less = input2
        for index, _ in enumerate(less):
            if less[index] != more[index]:
                return is_same(less[index:], more[index+1:])
        return True

    # if 0 length difference
    if len(input1) == len(input2):
        count = 0
        for index, _ in enumerate(input2):
            if input1[index] != input2[index]:
                count += 1
            if count > 1:
                return False
        return True
