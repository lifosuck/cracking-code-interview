# Is Unique: implement an algorithm to determine if a string has all unique characters
import unittest
from chapter1.hashmap import TrivialHash


class MyTest(unittest.TestCase):
    def test(self):
        self.assertTrue(brute_force("hi"))
        self.assertFalse(brute_force("hello world"))

        self.assertTrue(hash_map("hi"))
        self.assertFalse(hash_map("hello world"))


# brute force
# this is O(n^2) time complexity and O(n) space complexity
def brute_force(some_string):
    all_chars = []
    for c in some_string:
        for c_check in all_chars:
            if c == c_check:
                return False
        all_chars.append(c)
    return True


# hashmap
# this is O(n) time complexity and O(n) space complexity
def hash_map(some_string):
    hash = TrivialHash()
    for c in some_string:
        if hash.get(c) is not None:
            return False
        hash.append(c, True)
    return True

# text book idea : map char to number, and have an array, accessing array is O(1) thus the whole algorithm is O(n)

def is_all_unique_chars(some_string):
    brute_force(some_string)
