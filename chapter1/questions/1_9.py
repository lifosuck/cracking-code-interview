# String Rotation to see if s2 is s1's rotation by only using one call to isSubstring
import unittest

class MyTest(unittest.TestCase):
    def test(self):
        self.assertTrue(isRotationOf("waterbottle", "erbottlewat"))


def isSubstring(pattern, template):
    if pattern in template:
        return True
    return False


# is the pattern a rotation variation of the target
def isRotationOf(pattern, target):
    toCheck = pattern * 2
    return isSubstring(target, toCheck)
