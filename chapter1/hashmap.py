# simple division hashing

primeList = [1, 2, 3, 5, 7, 11, 13, 17, 19, 97, 191, 193, 197, 199, 389, 2179]


def find_prime(n):
    for i in primeList:
        if i >= n:
            return i


def convert_string_to_int(some_input):
    sum = 0
    for i in some_input:
        sum += ord(i)
    return sum


class TrivialHash:
    def __init__(self):
        self.internal_list = []
        self.current_hash_mod = 1
        self.size = 0

    def calculate_hash(self, item):
        return convert_string_to_int(item) % self.current_hash_mod

    def items(self):
        for bucket in self.internal_list:
            if bucket:
                for item in bucket:
                    if item:
                        yield item[0], item[1]

    def resize(self, expected_size):
        temp_new_list = []
        while len(temp_new_list) < expected_size:
            temp_new_list.append([])
        for i in self.internal_list:
            for val in i:
                key_hash = self.calculate_hash(val[0])
                temp_new_list[key_hash].append([val[0], val[1]])
        self.internal_list = temp_new_list

    def replace(self, k1, k2, value):
        if value is None:
            del self.internal_list[k1][k2]
            self.size -= 1
        self.internal_list[k1][k2][1] = value

    def get_index(self, key):
        index = self.calculate_hash(key)
        if len(self.internal_list) - 1 < index:
            return None, None
        if self.internal_list[index]:
            for k, v in enumerate(self.internal_list[index]):
                if v[0] == key:
                    return index, k

        return None, None

    def delete(self, key):
        i1, i2 = self.get_index(key)
        if i1 is None:
            return None
        thresh_hold = len(self.internal_list) * 3 // 4
        del self.internal_list[i1][i2]
        self.size -= 1
        if self.size < thresh_hold:
            self.current_hash_mod = find_prime(len(self.internal_list) // 2)
            self.resize(self.current_hash_mod)

    def append(self, key, value):
        i1, i2 = self.get_index(key)
        if i1 is not None:
            self.replace(i1, i2, value)
            return
        expected_size = self.size + 1
        if expected_size > self.current_hash_mod:
            self.current_hash_mod = find_prime(expected_size * 2)
            expected_size = self.current_hash_mod
        # if require to resize
        if len(self.internal_list) < expected_size:
            self.resize(expected_size)
        key_hash = self.calculate_hash(key)
        self.internal_list[key_hash].append([key, value])
        self.size += 1

    def get(self, key):
        i1, i2 = self.get_index(key)
        if i1 is None:
            return None
        return self.internal_list[i1][i2][1]
