from chapter1.resizeable_array import ResizeableArray, Null


class StringBuilder:
    def __init__(self):
        self.internal_array = ResizeableArray()

    def concatenate(self, *words):
        for word in words:
            for c in word:
                self.internal_array.append(c)

    def __str__(self):
        return "".join(c for c in self.internal_array if type(c) is not Null)
