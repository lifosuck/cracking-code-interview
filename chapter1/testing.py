import random
import string

from chapter1.hashmap import TrivialHash
from chapter1.resizeable_array import ResizeableArray, Null
from chapter1.string_builder import StringBuilder


def main():
    test_string_builder()
    test_resizeable_array()
    test_hashmap()


def test_string_builder():
    bunch_of_words = ["yo", "hello", "world"]
    builder = StringBuilder()
    builder.concatenate(*bunch_of_words)
    print(builder)
    pass


def test_resizeable_array():
    array = ResizeableArray()
    for i in range(100):
        array.append(i)
    print(array.size)
    print(array.capacity())
    array.insert(5, 10001)
    print(array)
    array.delete(5)
    print("new", array)
    for i in range(100):
        array.pop()
    print(array.size)
    print(array.capacity())
    print(array)


def test_hashmap():
    someUniqueStuff = [["hello", 5], ["blahblahblah", "yo"], ["meh", 1], ["meh", "SHOULD SEE THIS INSTEAD"]]

    hash = TrivialHash()
    for v in someUniqueStuff:
        hash.append(v[0], v[1])
    print(hash.get("hello"))
    print(hash.get("world"))
    print(hash.get("blahblahblah"))
    print(hash.get("meh"))
    print("before append large", len(hash.internal_list))
    print(hash.internal_list)
    gen = generateRandomStuff()
    lookupList = []
    for i in range(100):
        a = next(gen)
        b = next(gen)
        lookupList.append(a)
        hash.append(a, b)
    print(len(hash.internal_list))
    print("before del", len(hash.internal_list))
    for j in range(100):
        for i in lookupList:
            v = hash.get(i)
            if v is None:
                print("should gotten something")
    for i in lookupList:
        hash.delete(i)
    print("after del", len(hash.internal_list))
    collision_amount = 0
    for i in hash.internal_list:
        if len(i) > 1:
            print(len(i))
            collision_amount += 1
    print("collision amount", collision_amount)


def generateRandomStuff():
    while True:
        yield "".join(random.choices(string.ascii_letters + string.digits, k=10))


if __name__ == "__main__":
    main()
