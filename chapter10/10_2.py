def main():
    arrays_of_anagram = ['abc', 'def', 'hij', 'bca', 'cba', 'efd', 'hji']
    final = sort_arrays_of_anagram(arrays_of_anagram)
    print(final)
    pass


def sort_arrays_of_anagram(some_array):
    mapping = {}
    for word in some_array:
        sum = 0
        for char in word:
            sum += ord(char)
        if sum in mapping:
            mapping[sum].append(word)
        else:
            mapping[sum] = [word]
    final_array = []
    for _, v in mapping.items():
        final_array.extend(v)
    return final_array


if __name__ == "__main__":
    main()
