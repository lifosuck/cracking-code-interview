counter_swap = 0
counter_compare = 0


def main():
    my_array = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    my_array.sort()
    print(my_array)
    quick_sort(my_array)
    print(my_array)
    print(counter_swap)
    print(counter_compare)
    pass


def swap(some_array, index1, index2):
    global counter_swap
    counter_swap += 1
    some_array[index1], some_array[index2] = some_array[index2], some_array[index1]


def less_than(some_array, index1, index2):
    global counter_compare
    counter_compare += 1
    return some_array[index1] < some_array[index2]


def generate_swappable_index(some_array, left, right):
    pivot = some_array[right]
    while right > left:
        right -= 1
        if some_array[right] < pivot:
            yield right


def partition_array(some_array, left, right):
    pivot = some_array[right - 1]
    j = left - 1  # smallest element
    for i in range(left, right - 1):
        if some_array[i] <= pivot:
            j += 1
            swap(some_array, i, j)
    swap(some_array, j + 1, right - 1)
    return j + 1


def do_quick_sort(some_array, left, right):
    # find a pivot, use last element of the array
    # if array len is 1 , done.
    if right - left <= 1:
        return

    index = partition_array(some_array, left, right)
    do_quick_sort(some_array, left, index)
    do_quick_sort(some_array, index + 1, right)


def quick_sort(some_array):
    do_quick_sort(some_array, 0, len(some_array))
    return some_array


if __name__ == "__main__":
    array1 = [1, 2, 3]
    print(array1[0:len(array1)])
    main()
