counter = 0
class Listy:

    def get_val(self, index):
        if index < 1025:
            return 1
        if index == 1025:
            return 10
        if 1050 > index > 1025:
            return 11
        return -1


def find_lowest_avilable_index(listy, l_index, r_index):
    global counter
    counter +=1
    if listy.get_val(l_index) == -1:
        return None
    if listy.get_val(r_index) != -1:
        return r_index
    mid = (l_index + r_index) // 2
    if listy.get_val(mid) == -1:
        return find_lowest_avilable_index(listy, l_index + 1, mid - 1)
    return find_lowest_avilable_index(listy, mid + 1, r_index)


def find_index(listy, number, l_index, r_index):
    global counter
    counter +=1
    l_value = listy.get_val(l_index)
    if l_value == number:
        return l_index
    r_value = listy.get_val(r_index)
    if r_value == number:
        return r_index
    if 0 <= r_value < number:
        return find_index(listy, number, r_index + 1, r_index * 2)
    if r_value == -1:
        r_value = find_lowest_avilable_index(listy, l_index + 1, r_index - 1)
    if r_value is None:
        return None
    if l_value < number < r_value:
        mid = (l_index + r_index) // 2
        m_value = listy.get_val(mid)
        if m_value == number:
            return mid
        if m_value < number:
            return find_index(listy, number, mid + 1, r_index)
        return find_index(listy, number, l_index + 1, mid)
    return None


def main():
    listy = Listy()
    val = find_index(listy, 10, 0, 2)
    print(val)
    print(counter)
    pass


main()
