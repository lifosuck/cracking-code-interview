counter_swap = 0
counter_compare = 0


def main():
    my_array = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    merge_sort(my_array)
    print(my_array)
    print(counter_swap)
    print(counter_compare)
    pass


def swap(some_array, index1, index2):
    global counter_swap
    counter_swap += 1
    some_array[index1], some_array[index2] = some_array[index2], some_array[index1]


def less_than(some_array, index1, index2):
    global counter_compare
    counter_compare += 1
    return some_array[index1] < some_array[index2]


def merge(some_array, left_index, middle, right_index):
    print("mering at index", left_index, middle, right_index)
    temp_left = some_array[left_index: middle]
    print(temp_left)
    temp_right = some_array[middle: right_index]
    print(temp_right)
    array_index = left_index
    l_index = 0
    r_index = 0
    l_max = len(temp_left)
    r_max = len(temp_right)
    # if not temp_left or not temp_right:
    #     return
    #
    # while l_index < l_max and r_index < r_max:
    #     if temp_left[l_index] <= temp_right[r_index]:
    #         some_array[array_index] = temp_left[l_index]
    #         l_index += 1
    #     else:
    #         some_array[array_index] = temp_right[r_index]
    #         r_index += 1
    #     array_index += 1
    #
    # while r_index < r_max:
    #     some_array[array_index] = temp_right[r_index]
    #     r_index += 1
    #     array_index += 1
    #
    # while l_index < l_max:
    #     some_array[array_index] = temp_left[l_index]
    #     l_index += 1
    #     array_index += 1
    # print(some_array)
    # if base case, do have to do anything

    if not temp_left or not temp_right:
        return

    while l_index != l_max or r_index != r_max:
        if r_index == r_max or temp_left[l_index] < temp_right[r_index]:
            some_array[array_index] = temp_left[l_index]
            l_index += 1
        else:
            some_array[array_index] = temp_right[r_index]
            r_index += 1
        array_index += 1
    print(some_array)


def do_merge_sort(some_array, left_index, right_index):
    if (right_index - left_index) <= 1:
        return
    middle = (right_index + left_index) // 2
    do_merge_sort(some_array, left_index, middle)
    do_merge_sort(some_array, middle, right_index)
    merge(some_array, left_index, middle, right_index)


def merge_sort(some_array):
    do_merge_sort(some_array, 0, len(some_array))
    return some_array


if __name__ == "__main__":
    main()
