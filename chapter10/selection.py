counter_swap = 0
counter_compare = 0


def main():
    my_array = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    selection_sort(my_array)
    print(my_array)
    print(counter_swap)
    print(counter_compare)
    pass


def swap(some_array, index1, index2):
    global counter_swap
    counter_swap += 1
    some_array[index1], some_array[index2] = some_array[index2], some_array[index1]


def less_than(some_array, index1, index2):
    global counter_compare
    counter_compare += 1
    return some_array[index1] < some_array[index2]


def find_max_index(some_array):
    largest_index = 0
    for i, _ in enumerate(some_array):
        if i > 0:
            if not less_than(some_array, i, largest_index):
                largest_index = i
    return largest_index


def selection_sort(some_array):
    processed = 0

    for _ in some_array:
        temp = some_array[:len(some_array) - processed]
        largest_index = find_max_index(temp)
        swap(some_array, len(some_array) - 1 - processed, largest_index)
        processed += 1


if __name__ == "__main__":
    main()
