def main():
    rotated_array = [15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14]
    val = find_from_array(rotated_array, 0, len(rotated_array) - 1, 5)
    print(val)

    pass


def find_from_array(some_array, l_index, r_index, number):
    if r_index <= l_index:
        return None
    l_value = some_array[l_index]
    r_value = some_array[r_index]
    if l_value == number:
        return l_index
    if r_value == number:
        return r_index
    mid = (l_index + r_index) // 2
    m_value = some_array[mid]
    if m_value == number:
        return mid
    if l_value < number > m_value or l_value < number < m_value:
        return find_from_array(some_array, l_index + 1, mid, number)
    if l_value > r_value and l_value > number > r_value:
        return None
    return find_from_array(some_array, mid + 1, r_index, number)


if __name__ == "__main__":
    main()
    pass
