def main():
    large_array = [4, 5, 6, None, None, None]
    small_array = [1, 3, 7]
    final = merge(large_array, small_array)
    print(final)

    pass


def merge(large_array, small_array):
    current_index = len(large_array) - 1
    small_array_start_index = len(small_array) - 1
    large_array_start_index = 2

    while small_array_start_index >= 0 or large_array_start_index >= 0:
        if large_array[large_array_start_index] >= small_array[small_array_start_index] and (
                large_array_start_index >= 0) or small_array_start_index < 0:
            large_array[current_index],large_array[large_array_start_index] = large_array[large_array_start_index],large_array[current_index]
            large_array_start_index -= 1
        else:
            large_array[current_index] = small_array[small_array_start_index]
            small_array_start_index -= 1
        current_index -= 1
    return large_array


if __name__ == "__main__":
    main()
