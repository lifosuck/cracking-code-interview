import matplotlib.pyplot as plt



def main():
    my_array = [5, 3, 2, 1, 3, 5, 6, 1, 8]
    result = make_peaks_and_valleys(my_array)
    print(result)
    plt.plot(result)
    plt.plot(my_array)
    plt.show()


def make_peaks_and_valleys(some_array):
    if len(some_array) < 3:
        return some_array
    sorted_array = some_array.sort()
    r_index = len(some_array) - 1
    l_index = 0
    final_array = []
    flip = False
    while r_index > l_index:
        if flip:
            final_array.append(some_array[l_index])
            l_index += 1
        else:
            final_array.append(some_array[r_index])
            r_index -= 1
        flip = not flip
    return  final_array


if __name__ == "__main__":
    main()
