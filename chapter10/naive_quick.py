counter_swap = 0
counter_compare = 0


def main():
    my_array = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    my_array.sort()
    print(my_array)
    quick_sort(my_array)
    print(my_array)
    print(counter_swap)
    print(counter_compare)
    pass


def quick_sort(some_array):
    if len(some_array) <= 1:
        return []
    pivot = some_array[-1]
    small = []
    large = []
    pivots = []
    for i in some_array:
        if i < pivot:
            small.append(i)
        elif i > pivot:
            large.append(i)
        else:
            pivots.append(i)

    small_sorted = quick_sort(small)
    large_sorted = quick_sort(large)

    return [*small_sorted, *pivots, *large_sorted]


if __name__ == "__main__":
    array1 = [1, 2, 3]
    print(array1[0:len(array1)])
    main()
