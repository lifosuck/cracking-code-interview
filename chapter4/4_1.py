# Implement an algorithm to find kth to last element of a singly list array
import unittest


test_graph = {
    "A":["B"],
    "B":["A","C"],
    "C":["D"],
    "D":["E", "X","Y","Z"],
    "E":["F","A"],
    "G":["C"],
    "H":["G"],
    "X":[],
    "Y":[],
    "Z":[],
}

# Algorithm complexity ie. this is O(N) space/time algorithm
class MyTest(unittest.TestCase):
    def test(self):
        self.assertTrue(is_route_present(test_graph,"G","A", {}))
        self.assertFalse(is_route_present(test_graph,"A","G",{}))

        self.assertTrue(breadth_first_search(test_graph,"G","A",[],{}))
def get_node_children(graph,node_name):
    if node_name in graph:
        return graph[node_name]
    return None


# this is depth first search
def is_route_present(graph,node_a,node_b,visited):
    print(node_a,node_b)
    visited[node_a] = True
    if node_a == node_b:
        return True
    node_a_children = get_node_children(graph,node_a)
    if not node_a_children:
        return False
    for node in node_a_children:
        if node in visited:
            continue
        if is_route_present(graph,node,node_b,visited):
            return True
    return False

def breadth_first_search(graph,node_a,node_b,queue,visited):
    print(node_a,node_b)
    print(queue)
    if not visited:
        queue.append(node_a)
        print(queue)
    visited[node_a] = True
    if not queue:
        return False

    current_node = queue.pop()
    if current_node == node_b:
        return True
    children = get_node_children(graph,current_node)
    for child in children:
        if child not in visited:
            queue.append(child)

    return breadth_first_search(graph,node_a,node_b,queue,visited)


