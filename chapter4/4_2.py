# Implement an algorithm to find kth to last element of a singly list array
import unittest


# Algorithm complexity ie. this is O(N) space/time algorithm
class MyTest(unittest.TestCase):
    def test(self):
        test_sequence = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        node = construct_balanced_bst(test_sequence)
        inorder_travseral(node)
        preorder_traversal(node)
        postorder_traversal(node)



def construct_balanced_bst(sorted_sequence):
    # find the mid point, construct node:
    # recurse
    if len(sorted_sequence) == 1:
        return Node(sorted_sequence[0])
    if len(sorted_sequence) == 0:
        return None
    center_index = len(sorted_sequence) // 2
    left_index = sorted_sequence[:center_index]
    right_index = sorted_sequence[center_index + 1:]
    center_node = Node(sorted_sequence[center_index])
    center_node.left = construct_balanced_bst(left_index)
    center_node.right = construct_balanced_bst(right_index)
    return center_node


class Node:
    def __init__(self, value):
        self.val = value
        self.left = None
        self.right = None


def visit_node(node):
    print(node.val)


def inorder_travseral(node):
    # left center right
    if node is None:
        return
    inorder_travseral(node.left)
    visit_node(node)
    inorder_travseral(node.right)


def preorder_traversal(node):
    # center, left right
    if node is None:
        return
    visit_node(node)
    preorder_traversal(node.left)
    preorder_traversal(node.right)


def postorder_traversal(node):
    # left right center
    if node is None:
        return
    postorder_traversal(node.left)
    postorder_traversal(node.right)
    visit_node(node)
